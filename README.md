# README #

A basic user registration web application. 

### What is this repository for? ###

* Quick summary
	- The registration process contains 4 separated steps. Only one step is shown at a time to the customer.The user is able to leave the registration on every step/view until he finished the whole
registration successfully. Accordingly to this he should be redirected to the last opened step when he’s joining the
process again(re-navigating to the webpage). So the state of already inserted data needs to be saved.
	* View 1: Insert personal information
		- Firstname, lastname, telephone
	* View 2: Insert address information
		- Address including street, house number, zip code, city
	* View 3: Insert payment information
		- Account owner
		- IBAN (doesn’t need to be validated)
		- When clicking the “next” button, the inserted data is saved in configured database(mysql/sqlite).
	* View 4: Success page 	

### How do I get set up? ###

* Summary of set up
	- Download the repository to local machine.
	- Install xampp/wampp on local machine, edit httpd.cong file
	- Inside httpd.conf file, add project root folder path in <DocumentRoot >.
	- Open system console and go to project/web_app folder and run NPM install command
	- NPM install will add all the project dependencies in node_modules folder.
	- Import the mysql structure ( can be found as wunder_registration.sql in root directory) into any mysql client.
	- Run apache and mysql from xampp/wampp console, the apache service will be running on http://localhost:80
	- Inside web_app folder, run the npm start command to run the project on http://localhost:3000

* Dependencies 
	- PHP 5.6^
	- node 8.6^
	- mysql 5.7^
	
* Frameworks Used
	- CodeIgniter
	- ReactJS

### UI Snippets ###

![](https://bitbucket.org/arjun2691/wunderfleetreg/raw/6191c957eb1cdfcf4d7edf9debdb76d69277e928/personalInfo.PNG)
![](https://bitbucket.org/arjun2691/wunderfleetreg/raw/6191c957eb1cdfcf4d7edf9debdb76d69277e928/addressInfo.PNG)
![](https://bitbucket.org/arjun2691/wunderfleetreg/raw/6191c957eb1cdfcf4d7edf9debdb76d69277e928/paymentInfo.PNG)
![](https://bitbucket.org/arjun2691/wunderfleetreg/raw/6191c957eb1cdfcf4d7edf9debdb76d69277e928/successInfo.PNG)
	
### Questions ###

* Describe possible performance optimizations for your Code.
	- For validating session, http request is send to database. An in-memory cache(REDIS) can be used to store the session parameters.
	- Cookies are used to maintain session,  HttpOnly and secure flags can be used to make the cookies more secure. When a secure flag is used, then the cookie will only be sent over HTTPS, which is HTTP over SSL/TLS..
	- MVI architecure design pattern is used in the project, individual components can be dockerized to run on different enviroment.
	
* Which things could be done better, than you’ve done it?
	- An application state management tool ( Redux ) can be used to maintain the application state.
	- Better HTML form validations can be used.
	- UI components can be designed better.

* Which designed pattern is used and why ?
	- The project uses Model View Intent ( MVI ) architecure design pattern.
	* It provides separate functionality to develope and maintain independent, interchangeable modules.
	* It is Multi-platform standard, so whether it’s a Web or Android or IOS, same code can work.