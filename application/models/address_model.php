<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Address_model extends CI_Model {

    private $addressInfo = 'address_info';
	
    function getAddressInfo($userId) {
        $query = $this->db->get_where($this->addressInfo, array("userId" => $userId));
        if ($query) {
            return $query->row();
        }
        return NULL;
    }
	
    function addAddressInfo($userId,$street,$houseNo,$zipCode,$city) {
        $data = array('userId' => $userId, 'street' => $street, 'houseNo' => $houseNo, 'zipCode' => $zipCode,'city'=>$city);
        return $this->db->insert($this->addressInfo, $data);
    }
    function updateAddressInfo($userId,$street,$houseNo,$zipCode,$city) {
        $data = array('street' => $street, 'houseNo' => $houseNo, 'zipCode' => $zipCode,'city'=>$city);
        $this->db->where('userId', $userId);
        return $this->db->update($this->addressInfo, $data);
    }
    
    function deleteAddressInfo($addressInfoId) {
        $this->db->where('addressId', $addressInfoId);
        $this->db->delete($this->addressInfo);
    }

}