<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    private $userInfo = 'user_info';
	
    function getUserInfo($cookieValue) {
        $query = $this->db->get_where($this->userInfo, array("sessionId" => $cookieValue));
        if ($query) {
            return $query->row();
        }
        return NULL;
    }
	
    function addUserInfo($userId, $cookieValue,$firstName,$lastName,$phone) {
        $data = array('userId'=>$userId,'sessionId' => $cookieValue, 'firstName' => $firstName, 'lastName' => $lastName, 'phoneNo' => $phone);
        return $this->db->insert($this->userInfo, $data);
    }
    function updateUserInfo($userId,$firstName,$lastName,$phone) {
        $data = array('firstName' => $firstName, 'lastName' => $lastName, 'phoneNo' => $phone);
        $this->db->where('userId', $userId);
        return $this->db->update($this->userInfo, $data);
    }
    
    function deleteUserInfo($userInfoId) {
        $this->db->where('userId', $userInfoId);
        $this->db->delete($this->userInfo);
    }

}