<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Session_model extends CI_Model {

    private $session_info = 'session_info';
	
    function get_session_info($session_info_id) {
        $query = $this->db->get_where($this->session_info, array("sessionId" => $session_info_id));
        if ($query) {
            return $query->row();
        }
        return NULL;
    }
	
    function add_session_info($session_info_id, $step) {
        $data = array('sessionId' => $session_info_id, 'stepInfo' => $step);
        return $this->db->insert($this->session_info, $data);
    }
    function update_session_info($session_info_id, $step) {
        $data = array('stepInfo' => $step);
        $this->db->where('sessionId', $session_info_id);
        return $this->db->update($this->session_info, $data);
    }
    
    function delete_session_info($session_info_id) {
        $this->db->where('sessionId', $session_info_id);
        $this->db->delete($this->session_info);
    }

}