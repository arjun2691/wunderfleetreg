<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model {

    private $paymentInfo = 'payment_status';
	
    function getPaymentInfo($userId) {
        $query = $this->db->get_where($this->paymentInfo, array("userId" => $userId));
        if ($query) {
            return $query->row();
        }
        return NULL;
    }
	
    function addPaymentStatus($customerId, $iBan, $owner, $paymentId) {
        $data = array('userId' => $customerId, 'iBan' => $iBan, 'owner' => $owner, 'paymentId' => $paymentId);
        return $this->db->insert($this->paymentInfo, $data);
    }
    function updatePaymentInfo($customerId, $iBan, $owner, $paymentId) {
        $data = array('iBan' => $iBan, 'owner' => $owner, 'paymentId' => $paymentId);
        $this->db->where('userId', $customerId);
        return $this->db->update($this->paymentInfo, $data);
    }
    
    function deletePaymentInfo($paymentInfoId) {
        $this->db->where('userId', $paymentInfoId);
        $this->db->delete($this->paymentInfo);
    }

}