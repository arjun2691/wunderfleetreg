<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');


class Payment extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
    }

    function getPaymentInfo($userId = '') {
        $httpRequest = true;
        if(!$userId) {
            $userId = $this->input->post('userId');
        } else {
            $httpRequest = false;
        }

        $paymentExist = $this->payment_model->getPaymentInfo($userId);
        if($paymentExist && $httpRequest) {
            echo json_encode(array('response'=>$paymentExist));
        } else {
            return json_decode(json_encode($paymentExist),true);
        }
    }    

	public function addPaymentStatus() {
        $owner = $this->input->post('owner');
        $iBan = $this->input->post('iBan');
        $customerId = $this->input->post('customerId');
        if(!$owner || !$iBan) {
            echo json_encode(array('response'=>'failed'));
        }
        $params = array('customerId'=>$customerId,'iban'=>$iBan,'owner' => $owner );
        $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
        $response = json_decode($this->postPaymentStatus($url,$params),true);
        if($response['paymentDataId']) {
            $this->payment_model->addPaymentStatus($customerId, $iBan, $owner, $response['paymentDataId']);
            echo json_encode(array('status'=>'success','paymentId'=>$response['paymentDataId']));
        } else {
            echo json_encode(array('status'=>'failed','response'=>$response));
        }
    }

    public function postPaymentStatus($url,$params) {
        $postData = json_encode($params);
        $curl = curl_init();

         curl_setopt_array($curl, array(
           CURLOPT_URL => $url,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 0,
           CURLOPT_FOLLOWLOCATION => true,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS =>$postData,
           CURLOPT_HTTPHEADER => array(
             "Content-Type: application/json"
           ),
         ));
         
         $output=curl_exec($curl);
         return $output;        
    }
}
