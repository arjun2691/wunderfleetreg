<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');


class Address extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('address_model');
    }

    function getAddressInfo($userId = '') {
        $httpRequest = true;
        if(!$userId) {
            $userId = $this->input->post('userId');
        } else {
            $httpRequest = false;
        }

        $addressExist = $this->address_model->getAddressInfo($userId);

        if($addressExist && $httpRequest) {
            echo json_encode(array('response'=>$addressExist));
        } else {
            return json_decode(json_encode($addressExist),true);
        }
    }

	public function addAddressInfo() {
        $street = $this->input->post('street');
        $houseNo = $this->input->post('houseNo');
        $zipCode = $this->input->post('zipCode');
        $city = $this->input->post('city');
        $userId = $this->input->post('userId');

        if(!$userId) {
            echo json_encode(array('response'=>'failed'));
        }
        $addressExist = $this->getAddressInfo($userId);
        if(!$addressExist) {
            $result = $this->address_model->addAddressInfo($userId,$street,$houseNo,$zipCode,$city);
        } else {
            $result = $this->address_model->updateAddressInfo($addressExist['userId'],$street,$houseNo,$zipCode,$city);
        }
        if($result) {
            echo json_encode(array('response'=>'success'));
        } else {
            echo json_encode(array('response'=>'failed'));
        }
    }

}
