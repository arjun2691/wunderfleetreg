<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    private function generateUserId() {
        return rand() + time();
    }

	public function addUserInfo() {
        $cookieValue = $this->input->post('cookieValue');
        $firstName = $this->input->post('firstName');
        $lastName = $this->input->post('lastName');
        $phone = $this->input->post('phone');
        if(!$cookieValue) {
            echo json_encode(array('response'=>'failed'));
        }
        $userExist = $this->getUserInfo($cookieValue);
        if(!$userExist) {
            $userId = $this->generateUserId();
            $result = $this->user_model->addUserInfo($userId, $cookieValue,$firstName,$lastName,$phone);
        } else {
            $userId = $userExist['userId'];
            $result = $this->user_model->updateUserInfo($userId,$firstName,$lastName,$phone);
        }
        if($result) {
            echo json_encode(array('response'=>'success','userId'=> $userId));
        } else {
            echo json_encode(array('response'=>'failed'));
        }
    }
    function getUserInfo($cookieValue = '') {
        $httpRequest = true;
        if(!$cookieValue) {
            $cookieValue = $this->input->post('cookieValue');
        } else {
            $httpRequest = false;
        }

        $userExist = $this->user_model->getUserInfo($cookieValue);

        if($userExist && $httpRequest) {
            echo json_encode(array('response'=>$userExist));
        } else {
            return json_decode(json_encode($userExist),true);
        }
    }

}
