<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');


class Session extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('session_model');
    }

	public function addSession() {
        $cookieValue = $this->input->post('cookieValue');
        $step = $this->input->post('step');
        if($cookieValue) {
            $sessionExist = $this->getSessionInfo($cookieValue);
            if(!$sessionExist) {
                $result = $this->session_model->add_session_info($cookieValue,0);
            } else {
                $result = $this->session_model->update_session_info($cookieValue,$step);
            }
                if($result) {
                echo json_encode(array('response'=>'success'));
            } else {
                echo json_encode(array('response'=>'failed'));
            }
        } else {
            echo json_encode(array('response'=>'failed'));
        }
    }

    public function getSessionInfo($cookieValue = '') {
        $httpRequest = true;
        if(!$cookieValue){
            $cookieValue = $this->input->post('cookieValue');
        } else {
            $httpRequest = false;
        }
        $result = $this->session_model->get_session_info($cookieValue);
        if($result && $httpRequest) {
            echo json_encode(array('step'=>$result));
        } else {
            return json_decode(json_encode($result),true);
        }    
    }


}
