import React from 'react';
import axios from 'axios';
import { Result, message } from 'antd';
import Constants from '../utils/Constants';
let { baseUrl } = Constants;


class SuccessInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentId: null,
        };
    }

    componentDidMount = async () => {
        try {
            let paymentId = this.props.getStepData('step3');
            if (!paymentId) {
                let formdata = new FormData();
                formdata.append('userId', this.props.userId);
                let resp = await axios.post(`${baseUrl.url}/payment/getPaymentInfo`, formdata);
                paymentId = resp.data.response.paymentId;
            }
            this.setState({ paymentId });
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page')
        }
    }


    render() {
        const paymentId = this.props.getStepData('step3') || this.state.paymentId;
        let subTitle = "PaymentID: " + paymentId;
        return (
            <Result
                status="success"
                title="Successfully Registered to Wunder!"
                subTitle={subTitle}
            />
        );
    }
}

export default SuccessInfo;