import React from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import {
    Form,
    Input,
    Button,
    message
} from 'antd';
import Constants from '../utils/Constants';
import FormUtils from '../utils/FormUtils';
let { formItemLayout, tailFormItemLayout } = FormUtils;

let { cookieName, baseUrl } = Constants;


class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formValues: null,
        };
    }
    handleSubmit = async e => {
        e.preventDefault();
        try {
            let resp = await this.props.form.validateFieldsAndScroll();
            this.setState({ formValues: resp });
            this.props.storeStepData('step0', resp);
            await this.updateUserInfo(resp);
            let cookieValue = Cookies.get(cookieName.name);
            this.props.updateCookie(cookieValue, 1);
            this.props.next();
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page');
        }
    };

    updateUserInfo = async (formValues) => {
        try {
            var formdata = new FormData();
            formdata.append('firstName', formValues.firstName);
            formdata.append('lastName', formValues.lastName);
            formdata.append('phone', formValues.phoneNo);
            formdata.append('cookieValue', Cookies.get(cookieName.name));
            let result = await axios.post(`${baseUrl.url}/user/addUserInfo`, formdata);
            if (result.data.userId) {
                this.props.saveUserInfo(result.data.userId);
            }
        } catch (err) {
            throw new Error(err);
        }
    }

    componentDidMount = async () => {
        try {
            let formValues = this.props.getStepData('step0');
            if (!formValues) {
                let formdata = new FormData();
                formdata.append('cookieValue', Cookies.get(cookieName.name));
                let resp = await axios.post(`${baseUrl.url}/user/getUserInfo`, formdata);
                formValues = resp.data.response;
            }
            this.setState({ formValues });
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page');
        }
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        const { formValues } = this.state;

        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item
                    label={
                        <span>
                            First Name&nbsp;
                        </span>
                    }
                >
                    {getFieldDecorator('firstName', {
                        initialValue: (formValues && formValues.firstName) ? formValues.firstName : '',
                        rules: [{ required: true, message: 'Please input your First Name!', whitespace: true }],
                    })(<Input />)}
                </Form.Item>
                <Form.Item
                    label={
                        <span>
                            Last Name&nbsp;
                        </span>
                    }
                >
                    {getFieldDecorator('lastName', {
                        initialValue: (formValues && formValues.lastName) ? formValues.lastName : '',
                        rules: [{ required: true, message: 'Please input your Last Name!', whitespace: true }],
                    })(<Input />)}
                </Form.Item>

                <Form.Item label="Phone Number">
                    {getFieldDecorator('phoneNo', {
                        initialValue: (formValues && formValues.phoneNo) ? formValues.phoneNo : '',
                        rules: [{ required: true, message: 'Please input your phone number!' }],
                    })(<Input />)}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Next
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedUserInfo = Form.create({ name: 'register' })(UserInfo);
export default WrappedUserInfo;