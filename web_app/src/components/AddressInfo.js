import React from 'react';
import axios from 'axios';
import {
    Form,
    Input,
    Button,
    message
} from 'antd';
import Constants from '../utils/Constants';
import FormUtils from '../utils/FormUtils';
let { formItemLayout, tailFormItemLayout } = FormUtils;

let { baseUrl } = Constants;

class AddressInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formValues: null,
        };
    }
    handleSubmit = async e => {
        e.preventDefault();
        try {
            let resp = await this.props.form.validateFieldsAndScroll();
            this.setState({ formValues: resp });
            this.props.storeStepData('step1', resp);
            await this.updateAddressInfo(resp);
            this.props.updateCookie('', 1);
            this.props.next();
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page');
        }
    };
    updateAddressInfo = async (formValues) => {
        try {
            var formdata = new FormData();
            formdata.append('street', formValues.street);
            formdata.append('houseNo', formValues.houseNo);
            formdata.append('zipCode', formValues.zipCode);
            formdata.append('city', formValues.city);
            formdata.append('userId', this.props.userId);
            await axios.post(`${baseUrl.url}/address/addAddressInfo`, formdata);
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page')
        }
    }

    handlePrevious = () => {
        this.props.updateCookie('', -1);
        this.props.prev();
    }

    componentDidMount = async () => {
        try {
            let formValues = this.props.getStepData('step1');
            if (!formValues) {
                let formdata = new FormData();
                formdata.append('userId', this.props.userId);
                let resp = await axios.post(`${baseUrl.url}/address/getAddressInfo`, formdata);
                formValues = resp.data.response;
            }
            this.setState({ formValues });
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page');
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { formValues } = this.state;
        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item
                    label={
                        <span>
                            Street&nbsp;
                        </span>
                    }
                >
                    {getFieldDecorator('street', {
                        initialValue: (formValues && formValues.street) ? formValues.street : '',
                        rules: [{ required: true, message: 'Please input your Street Name!', whitespace: true }],
                    })(<Input />)}
                </Form.Item>
                <Form.Item
                    label={
                        <span>
                            House No.&nbsp;
                        </span>
                    }
                >
                    {getFieldDecorator('houseNo', {
                        initialValue: (formValues && formValues.houseNo) ? formValues.houseNo : '',
                        rules: [{ required: true, message: 'Please input your House Number!', whitespace: true }],
                    })(<Input />)}
                </Form.Item>
                <Form.Item
                    label={
                        <span>
                            Zip Code&nbsp;
                        </span>
                    }
                >
                    {getFieldDecorator('zipCode', {
                        initialValue: (formValues && formValues.zipCode) ? formValues.zipCode : '',
                        rules: [{ required: true, message: 'Please input your ZIP Code!', whitespace: true }],
                    })(<Input />)}
                </Form.Item>
                <Form.Item
                    label={
                        <span>
                            City&nbsp;
                        </span>
                    }
                >
                    {getFieldDecorator('city', {
                        initialValue: (formValues && formValues.city) ? formValues.city : '',
                        rules: [{ required: true, message: 'Please input your City Name!', whitespace: true }],
                    })(<Input />)}
                </Form.Item>
                {this.props.prev && <Form.Item {...tailFormItemLayout}>
                    <Button type="danger" onClick={this.handlePrevious}>
                        Previous
                    </Button>
                    {' '}
                    <Button type="primary" htmlType="submit">
                        Next
                    </Button>

                </Form.Item>}

            </Form>
        );
    }
}

const WrappedAddressInfo = Form.create({ name: 'register' })(AddressInfo);

export default WrappedAddressInfo;