import React from 'react';
import axios from 'axios';
import {
    Form,
    Input,
    Button,
    message
} from 'antd';
import Constants from '../utils/Constants';
import FormUtils from '../utils/FormUtils';
let { formItemLayout, tailFormItemLayout } = FormUtils;

let { baseUrl } = Constants;
class PaymentInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formValues: null,
            loading: false
        };
    }

    handleSubmit = async e => {
        e.preventDefault();
        try {
            let resp = await this.props.form.validateFieldsAndScroll();
            this.setState({ formValues: resp });
            this.props.storeStepData('step2', resp);
            await this.updatePaymentInfo(resp);
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page');
        }

    }
    componentDidMount = async () => {
        try {
            let formValues = this.props.getStepData('step2');
            if (!formValues) {
                let formdata = new FormData();
                formdata.append('userId', this.props.userId);
                let resp = await axios.post(`${baseUrl.url}/payment/getPaymentInfo`, formdata);
                if (resp.data.response)
                    formValues = { ...resp.data.response };
            }
            this.setState({ formValues });
        } catch (err) {
            message.error('Something went wrong!. Please refresh the page');
        }
    }


    updatePaymentInfo = async (formValues) => {
        try {
            var formdata = new FormData();
            formdata.append('iBan', formValues.iBan);
            formdata.append('owner', formValues.owner);
            formdata.append('customerId', this.props.userId);
            this.setState({ loading: true });
            let resp = await axios.post(`${baseUrl.url}/payment/addPaymentStatus`, formdata);
            if (resp && resp.data && resp.data.status === "success") {
                this.setState({ loading: false });
                this.props.storeStepData('step3', resp.data.paymentId);
                this.props.updateCookie('', 1);
                this.props.next();
            } else {
                throw new Error('Unable to process');
            }
        } catch (err) {
            throw new Error(err);
        }
    }


    handlePrevious = () => {
        this.props.updateCookie('', -1);
        this.props.prev();
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        const formValues = this.state.formValues || this.props.getStepData('step2');
        const { loading } = this.state;

        return (
            <>
                {loading && <div className="loading"></div>}
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Form.Item
                        label={
                            <span>
                                Account Owner&nbsp;
                        </span>
                        }
                    >
                        {getFieldDecorator('owner', {
                            initialValue: (formValues && formValues.owner) ? formValues.owner : '',
                            rules: [{ required: true, message: 'Please input your Account Name!', whitespace: true }],
                        })(<Input />)}
                    </Form.Item>
                    <Form.Item
                        label={
                            <span>
                                iBan&nbsp;
                        </span>
                        }
                    >
                        {getFieldDecorator('iBan', {
                            initialValue: (formValues && formValues.iBan) ? formValues.iBan : '',
                            rules: [{ required: true, message: 'Please input your iBan!', whitespace: true }],
                        })(<Input />)}
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="danger" onClick={this.handlePrevious}>
                            Previous
                    </Button>
                        {' '}{' '}
                        <Button type="primary" htmlType="submit">
                            Done
                    </Button>
                    </Form.Item>
                </Form>
            </>
        );
    }
}

const WrappedPaymentInfo = Form.create({ name: 'register' })(PaymentInfo);

export default WrappedPaymentInfo;