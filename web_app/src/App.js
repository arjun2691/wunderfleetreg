import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Steps, PageHeader, message } from 'antd';
import '../src/index.css';
import UserInfo from './components/UserInfo.js'
import AddressInfo from './components/AddressInfo.js'
import PaymentInfo from './components/PaymentInfo.js'
import SuccessInfo from './components/SuccessInfo.js'
import Constants from './utils/Constants';
let { cookieName, baseUrl } = Constants;
const { Step } = Steps;


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      sessionId: null,
      userId: null
    };
  }

  next = () => {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev = () => {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  saveUserInfo = (userId) => {
    this.setState({ userId })
  }
  updateCookie = async (cookieValue = '', step) => {
    try {
      if (!cookieValue) {
        cookieValue = this.state.sessionId;
        step = this.state.current + step;
      }
      var formdata = new FormData();
      formdata.append('cookieValue', cookieValue);
      formdata.append('step', step);
      await axios.post(`${baseUrl.url}/session/addSession`, formdata);
    } catch (err) {
      throw new Error(err);
    }
  }

  getStepInfo = async (cookieValue) => {
    try {
      var formdata = new FormData();
      formdata.append('cookieValue', cookieValue);
      formdata.append('step', this.state.current);
      let resp = await axios.post(`${baseUrl.url}/session/getSessionInfo`, formdata);
      if (!resp.data.step || resp.data.response === 'failed') {
        return 0;
      }
      await this.getUserInfo(cookieValue);
      return Number(resp.data.step.stepInfo);
    } catch (err) {
      message.error('Something went wrong!. Please refresh the page')
      this.setState({ current: 0 });
    }
  }

  getUserInfo = async (cookieValue) => {
    try {
      var formdata = new FormData();
      formdata.append('cookieValue', cookieValue);
      let resp = await axios.post(`${baseUrl.url}/user/getUserInfo`, formdata);
      if (resp.data.response === 'failed') {
        this.setState({ current: 0 })
      }
      if (resp.data.response.userId) {
        this.setState({ userId: resp.data.response.userId })
      }
    } catch (err) {
      message.error('Something went wrong!. Please refresh the page')
      this.setState({ current: 0 });
    }
  }

  storeStepData = async (step, formData) => {
    this.setState({
      [step]: formData
    });
  }

  getStepData = stepName => (stepName ? this.state[stepName] : this.state);

  componentDidMount = async () => {
    try {
      let isCookieSet = Cookies.get(cookieName.name);
      let cookieValue = '';
      if (!isCookieSet) {
        cookieValue = new Date().valueOf();
        Cookies.set(cookieName.name, cookieValue, { expires: 7 });
        await this.updateCookie(cookieValue, 0);
      } else {
        cookieValue = Cookies.get(cookieName.name);
        let resp = await this.getStepInfo(cookieValue);
        this.setState({ current: resp });
      }
      this.setState({ sessionId: cookieValue });
    } catch (err) {
      message.error('Something went wrong!. Please refresh the page');
      this.setState({ current: 0 });
    }
  }

  render() {
    const { current, userId } = this.state;
    const steps = [
      {
        title: 'Personal Info',
        component: (
          <UserInfo
            next={this.next}
            storeStepData={this.storeStepData}
            updateCookie={this.updateCookie}
            getStepData={this.getStepData}
            saveUserInfo={this.saveUserInfo}
          />
        )
      },
      {
        title: 'Address Info',
        component: (
          <AddressInfo
            next={this.next}
            prev={this.prev}
            storeStepData={this.storeStepData}
            updateCookie={this.updateCookie}
            userId={userId}
            getStepData={this.getStepData}
          />
        )
      },
      {
        title: 'Payment',
        component: (
          <PaymentInfo
            prev={this.prev}
            next={this.next}
            storeStepData={this.storeStepData}
            updateCookie={this.updateCookie}
            getStepData={this.getStepData}
            userId={userId}
          />
        )
      },
      {
        title: 'Success!',
        component: (
          <SuccessInfo
            prev={this.prev}
            storeStepData={this.storeStepData}
            userId={userId}
            getStepData={this.getStepData}
          />
        )
      },
    ];

    return (
      <div className="App-background">
        <div className="App-container" >
          <PageHeader
            style={{
              borderBottom: '1px solid rgb(235, 237, 240)',
              backgroundColor: '#fafafa',
              marginBottom: "5%",
              marginTop: "2%"
            }}
            title="Wunder Fleet Registration"

          />

          <Steps current={current}>
            {steps.map(item => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div className="steps-content">{steps[current].component}</div>

        </div>
      </div >

    );
  }
}

export default App;
