
const cookieName = {
    name: '_name_wunder_init_default_'
}

const baseUrl = {
    url: 'http://localhost:80'
}
const Constants = {
    cookieName,
    baseUrl
}
export default Constants;
